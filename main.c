#include "mysql.h"
#include "errmsg.h"

#include "stdio.h"
#include "string.h"
#include "ctype.h"
#include "stdint.h"


#define BUFFER_SIZE 30
#define QUERY_SIZE 100 // TODO: This has caused issues once (when was size 50). May want to think of a better way.

#define STRING_MAX 128 // TODO: Do I want 128?

#define STANDARDOUTPUT stdout


// TODO: Make all of these distinctive for globals? (all caps, etc.)
// TODO: Use Windows function to find out? Maybe use screen width instead (for file).
int terminalWidth = 75;

const char* myDefaultDBStr = "equipmentManagement";
const char* myDatabaseStr = "food";
const char* myHostStr = "localhost";
const char* myUserStr = "root";
const char* myPasswordStr = "root";
unsigned int port = 3306;
const char* unix_socket = "";
unsigned long client_flag = 0;

typedef _Bool uint1_t;

// TODO: Make this absolute path for safety?
const char* list_filename = "Lists/my_first_list.txt";
const char* list_mode = "w";

typedef struct UserStruct {
    char username[BUFFER_SIZE];
    char password[BUFFER_SIZE];
    char* permissions;
} User;

typedef struct Item {
    char* name;
    double price;
    int isle;
} Item;

// TODO: Maybe add attribute_names and num_attributes??...
typedef struct StoreList {
    Item* item;
    int num_items;
    double price;
    int shop_time;
} StoreList;

typedef struct DB_Entry {
    char* entry;
    int num_values;
} DB_Entry;


// Miscellaneous functions
void ValidateObject(void* object);

// Functions dealing with the Database
void PrettyFormat(FILE* fileStream, int terminalWidth);
void DisconnectAndDie(MYSQL* mySQL);
void PrintDetailsForDB(MYSQL* mySQL);
void HandleQuery(MYSQL* mySQL, char* query);
void CreateDB(MYSQL* mySQL, const char* databaseName);
void DeleteDB(MYSQL* mySQL, const char* databaseName);
void SelectDB(MYSQL* mySQL, const char* databaseName);
void CreateUserInDB(MYSQL* mySQL, User* user, const char* host, const char* privilegeStr);
void DeleteUserFromDB(MYSQL* mySQL, char* username, const char* host); 
// TODO: Perhaps have this printed to a file as well, using a FILE structure passed to to like I did for PrintList?
void PrintTableContents(MYSQL* mySQL, const char* databaseName, const char* columnNames, const char* tableName);
void GetTuple(MYSQL* mySQL, MYSQL_RES* result, enum_field_types* typeArray, char*** values);
void QuoteValues(char** values, enum_field_types* typeArray, int numFields);
void AddTupleToDB(MYSQL* mySQL, const char* databaseName, const char* tableName);
void PerformDummyQuery(MYSQL* mySQL, MYSQL_RES** result, const char* databaseName, const char* columnNames, const char* tableName);
unsigned long* FindLongestColumnEntry(MYSQL_RES* result);
int ValidValueFormat(char* name, enum_field_types type, char* value);

// Functions dealing with store lists
StoreList* CreateList();
Item* CreateItem(const char* name, double price, int isle);
void DeleteItem(Item* item);
void AddItem(StoreList* storeList, Item* item);
void SaveList(StoreList* storeList, const char* filename, const char* mode);
StoreList* ParseToList(char* data, int size);
StoreList* LoadList(const char* filename);
void RemoveFromList(StoreList* storeList, const char* name);
void AddListContentToDB(StoreList* storeList);
void PrintList(StoreList* storeList, FILE* fileStream);
void DeleteList(StoreList* storeList);
void ValidateFile(FILE* fp, const char* filename);

int main(int argc, char** argv)
{
    MYSQL* mySQL = mysql_init(NULL);

    if(!mySQL)
    {
        printf("mySQL object failed to initialize!\n");
        printf("ERROR: %s\n", mysql_error(mySQL));
        exit(1);
    }


    StoreList* storeList = CreateList();
    Item* item = CreateItem("Muffin", 2.23, 3);
    AddItem(storeList, item);
    item = CreateItem("Jeff", 1.339, 1);
    AddItem(storeList, item);
    item = CreateItem("Doughnut", 1.339, 1);
    AddItem(storeList, item);
    item = CreateItem("Cookie", 1.339, 1);
    AddItem(storeList, item);
    RemoveFromList(storeList, "Cookie");
    PrintList(storeList, stdout);


    SaveList(storeList, list_filename, list_mode);
    StoreList* loadedList = LoadList(list_filename);
    PrintList(loadedList, stdout);

    DeleteList(storeList);
    DeleteList(loadedList);
    printf("DONE!\n");

    // NOTE: Useful function? -> mysql_options(...)

    // Connect to MySQL database engine on specified host. Use any options previously set.
    if(!mysql_real_connect(mySQL, myHostStr, myUserStr, myPasswordStr, myDefaultDBStr, 0, NULL, 0))
    {
        printf("Failed to connect to MySQL database engine running on %s!\n", myHostStr);
        printf("ERROR: %s\n", mysql_error(mySQL));
    }

    // Create the Database if it does not exist yet, then select it as our active database
    CreateDB(mySQL, myDatabaseStr);
    SelectDB(mySQL, myDatabaseStr);

    PrintDetailsForDB(mySQL);
    PrintTableContents(mySQL, "hospital", "*", "stay");
    //AddTupleToDB(mySQL, "hospital", "stay");

    //DeleteDB(mySQL, myDatabaseStr);

    User* user = malloc(sizeof(User));
    //CreateUserInDB(mySQL, user, myHostStr, NULL);
    //DeleteUserFromDB(mySQL, user->username, myHostStr);
   
    free(user);
    mysql_close(mySQL);
    printf("End of main!\n");

    return 0;
}

void ValidateObject(void* object)
{
    if(!object)
    {
        printf("Failed to allocate memory!\n");
        exit(1); // NOTE: We are leaving mySQL hanging, but I don't wanna pass the object to so many function just for this...
    }
}

void ValidateFile(FILE* fp, const char* filename)
{
    if(!fp)
    {
        printf("Failed to open file: %s!\n", filename);
        exit(1); // NOTE: We are leaving mySQL hanging, but I don't wanna pass the object to so many function just for this...
    }
}

void PrettyFormat(FILE* fileStream, int terminalWidth)
{
    fprintf(fileStream, "\n");
    while(terminalWidth-- > 0)
    {
        fprintf(fileStream, "-");
    }
    fprintf(fileStream, "\n");
}

void DisconnectAndDie(MYSQL* mySQL)
{
    printf("ATTEMPTING TO CLOSE MYSQL BEFORE DYING\n");
    mysql_close(mySQL);
    printf("Dying now...\n");
    exit(1);
}

void SelectDB(MYSQL* mySQL, const char* databaseName)
{
    if(mysql_select_db(mySQL, databaseName))
    {
        printf("Failed to select %s database!\n", databaseName);
        printf("ERROR: %s\n", mysql_error(mySQL));
        DisconnectAndDie(mySQL);
    }
}

void CreateDB(MYSQL* mySQL, const char* databaseName)
{
    char query[QUERY_SIZE];
    sprintf(query, "CREATE DATABASE IF NOT EXISTS %s", databaseName);
    HandleQuery(mySQL, query);

    printf("Database %s now exists!\n", databaseName);
}

void DeleteDB(MYSQL* mySQL, const char* databaseName)
{
    char query[QUERY_SIZE];
    sprintf(query, "DROP DATABASE IF EXISTS %s", databaseName);
    HandleQuery(mySQL, query);

    printf("Database %s no longer exists!\n", databaseName);
}

void PrintDetailsForDB(MYSQL* mySQL)
{
    printf("MySQL Host information: %s\n", mysql_get_host_info(mySQL));
    printf("MySQL client version: %s\n", mysql_get_client_info());
}

void EncryptPassword(MYSQL* mySQL, User* user)
{
    char query[QUERY_SIZE];

    sprintf(query, "SET PASSWORD FOR '%s'@'%s' = PASSWORD('%s')", user->username, myHostStr, user->password);
    HandleQuery(mySQL, query);

    printf("Password encrypted successfully!\n");
}

// Handle a query to MySQL Server. Includes error checking and log statements.
void HandleQuery(MYSQL* mySQL, char* query)
{
    printf("QUERY: %s\n", query);
    if(mysql_query(mySQL, query))
    {
        printf("Query Failed!\n");
        printf("ERROR: %s\n", mysql_error(mySQL));
        DisconnectAndDie(mySQL);
    }
    printf("Query Succeeded!\n");
}

void CreateUserInDB(MYSQL* mySQL, User* user, const char* host, const char* privilegeStr)
{
    printf("Enter in username for the new user: ");
    fgets(user->username, BUFFER_SIZE, stdin);

    printf("Enter in the password for the new user: ");
    fgets(user->password, BUFFER_SIZE, stdin);

    char query[QUERY_SIZE];
    sprintf(query, "CREATE USER IF NOT EXISTS '%s'@'%s' IDENTIFIED BY '%s'", user->username, host, user->password);
    HandleQuery(mySQL, query);

    // Encrypt password for user
    EncryptPassword(mySQL, user);

    // Empty the query array to be used again
    memset(query, 0, QUERY_SIZE);

    // Flush privileges to tell server to reload grant tables
    sprintf(query, "FLUSH PRIVILEGES");
    HandleQuery(mySQL, query);

    printf("User with username %s is now in the database!\n", user->username);
}

void DeleteUserFromDB(MYSQL* mySQL, char* username, const char* host)
{
    char query[QUERY_SIZE];
    sprintf(query, "DROP USER IF EXISTS '%s'@'%s'", username, host);
    HandleQuery(mySQL, query);

    printf("User with username %s is now not in the database!\n", username);
}

unsigned long* FindLongestColumnEntry(MYSQL_RES* result)
{
    int num_fields = mysql_num_fields(result);
    unsigned long* longest_col_lines = malloc(sizeof(unsigned long) * num_fields);

    MYSQL_FIELD* field;
    for(int i = 0; i < num_fields; i++)
    {
        field = mysql_fetch_field(result);
        longest_col_lines[i] = strlen(field->name);
    }

    MYSQL_ROW row;
    while(row = mysql_fetch_row(result))
     {
        unsigned long* lengths = mysql_fetch_lengths(result);
        for(int i = 0; i < num_fields; i++)
        {
            if(lengths[i] > longest_col_lines[i])
            {
                longest_col_lines[i] = lengths[i];
            }
        }
     }

     return longest_col_lines;
}

void PerformDummyQuery(MYSQL* mySQL, MYSQL_RES** result, const char* databaseName, const char* columnNames, const char* tableName)
{
    printf("Performing dummy query...\n");

    char query[QUERY_SIZE];
    sprintf(query, "SELECT %s FROM %s.%s", columnNames, databaseName, tableName);
    HandleQuery(mySQL, query);
    *result = mysql_store_result(mySQL);
}

void PrintTableContents(MYSQL* mySQL, const char* databaseName, const char* columnNames, const char* tableName)
{
    MYSQL_RES* dummy_result;
    PerformDummyQuery(mySQL, &dummy_result, databaseName, columnNames, tableName);

    unsigned long* lengths = FindLongestColumnEntry(dummy_result);
    mysql_free_result(dummy_result);

    MYSQL_RES* result;
    PerformDummyQuery(mySQL, &result, databaseName, columnNames, tableName);
    
    if(result == NULL)
        DisconnectAndDie(mySQL);

    int num_fields = mysql_num_fields(result);

    MYSQL_ROW row;
    MYSQL_FIELD* field;

    int rows = mysql_num_rows(result);
    
    printf("\n");
    while(row = mysql_fetch_row(result))
    {
        int len;
        //unsigned long* lengths = mysql_fetch_lengths(result);
        for(int i = 0; i < num_fields; i++)
        {
            if(i == 0)
            {
                int j = 0;
                
                while(field = mysql_fetch_field(result))
                {
                    len = lengths[j++] + 1; // Some buffer space so things line up nicely
                    printf("%-*s ", len, field->name);
                }
                printf("\n");
            }

            len = lengths[i];
            printf("%-*s ", len + 1, row[i] ? row[i] : "NULL");
        }
    }
    printf("\n");

    free(lengths);
    mysql_free_result(result);
}


// Check if value is of expected format for the given field's type.
int ValidValueFormat(char* name, enum_field_types type, char* value)
{
    int result = 1;
    printf("\n");

    switch(type)
    {
        case FIELD_TYPE_TINY:
        case FIELD_TYPE_SHORT:
        case FIELD_TYPE_LONG:
        case FIELD_TYPE_INT24:
        case FIELD_TYPE_LONGLONG:
        {
            for(int i = 0; i < strlen(value); i++)
            {
                if(!isdigit(value[i]))
                {
                    printf("Expected integer values but %c is not an integer!\n", value[i]);
                    result = 0;
                    break;
                }
            }
        } break;

        case FIELD_TYPE_DECIMAL:
        case FIELD_TYPE_FLOAT:
        case FIELD_TYPE_DOUBLE:
        {
            for(int i = 0; i < strlen(value); i++)
            {
                if(!isdigit(value[i]) && value[i] != '.')
                {
                    printf("Expected decimal values but %c is not a valid decimal value!\n", value[i]);
                    result = 0;
                    break;
                }
            }
        } break;

        case FIELD_TYPE_YEAR:
        {
            int allowed_len = 4;
            if(strlen(value) != allowed_len)
            {
                printf("The year must be %i characters long! You entered: %s with a length of %i.\n", allowed_len, value, strlen(value));
                result = 0;
            }
            else if(!isdigit(value[0]) || !isdigit(value[1]), !isdigit(value[2]), !isdigit(value[3]))
            {
                printf("The year must be all digits, you entered %s!\n", value);
                result = 0;
            }
        } break;

        case FIELD_TYPE_DATE:
        {
            int allowed_len = 10;
            if(strlen(value) != allowed_len)
            {
                printf("The date must be %i characters long! You entered: %s with a length of %i.\n", allowed_len, value, strlen(value));
                result = 0;
            }
            else if((!isdigit(value[0]) || !isdigit(value[1]) || !isdigit(value[2]) || !isdigit(value[3])) || (value[4] != '-') 
                        || (!isdigit(value[5]) || !isdigit(value[6])) || (value[7] != '-') || (!isdigit(value[8]) || !isdigit(value[9])) )
            {
                printf("The date must be all digits with 2 dashes, you entered %s!\n", value);
                result = 0;
            }
        } break;

        case FIELD_TYPE_TIME:
        {
            int allowed_len = 8;
            if(strlen(value) != allowed_len)
            {
                printf("The time must be %i characters long! You entered %s with a length of %i.\n", allowed_len, value, strlen(value));
            }
            else if((!isdigit(value[0]) || !isdigit(value[1])) || (value[2] != ':') || (!isdigit(value[3]) 
                || !isdigit(value[4])) || (value[5] != ':') || (!isdigit(value[6]) || !isdigit(value[7])))
            {
                printf("The time must be all digits with 2 colons, you entered %s!\n", value);
            }
        } break;

        case FIELD_TYPE_DATETIME:
        {
            int allowed_len = 19;
            if(strlen(value) != allowed_len)
            {
                printf("The date must be %i characters long! You entered: %s with a length of %i.\n", allowed_len, value, strlen(value));
                result = 0;
            }
            else if( (!isdigit(value[0]) || !isdigit(value[1]) || !isdigit(value[2]) || !isdigit(value[3])) || (value[4] != '-') 
                        || (!isdigit(value[5]) || !isdigit(value[6])) || (value[7] != '-') || (!isdigit(value[8]) || !isdigit(value[9])) 
                        || (value[10] != ' ') || (!isdigit(value[11]) || !isdigit(value[12])) || (value[13] != ':') || (!isdigit(value[14]) 
                        || !isdigit(value[15])) || (value[16] != ':') || (!isdigit(value[17]) || !isdigit(value[18])) )
            {
                printf("The date must be all digits with 2 dashes, you entered %s!\n", value);
                result = 0;
            }
        } break;

        case FIELD_TYPE_STRING:
        {
            printf("STRING");
        } break;

        case FIELD_TYPE_NULL:
        {

        } break;

        default:
        {
            printf("The type (enum value of: %i) for %s is unsupported!\n", type, name);
            result = 0;
        } break;
    }
    

    return result;
}

void GetTuple(MYSQL* mySQL, MYSQL_RES* result, enum_field_types* typeArray, char*** values)
{
    MYSQL_FIELD* field;

    int num_fields = mysql_num_fields(result);

    char* value = malloc(sizeof(char) * STRING_MAX);

    char** database_attributes = malloc(sizeof(char*) * num_fields);

    int invalid_entry = 1;
    int i = 0;

    PrettyFormat(STANDARDOUTPUT, terminalWidth);
    
    while(field = mysql_fetch_field(result))
    {
        database_attributes[i] = malloc(sizeof(char) * (strlen(field->name) + 1));
        strcpy(database_attributes[i], field->name);
        printf("Attribute name: %s\n", database_attributes[i]);

        typeArray[i] = field->type;

        do
        {
            printf("Enter in value with format: ");
            switch(field->type)
            {
                case FIELD_TYPE_TINY:
                case FIELD_TYPE_SHORT:
                case FIELD_TYPE_LONG:
                case FIELD_TYPE_INT24:
                case FIELD_TYPE_LONGLONG:
                {
                    printf("An integer value");
                } break;

                case FIELD_TYPE_DECIMAL:
                case FIELD_TYPE_FLOAT:
                case FIELD_TYPE_DOUBLE:
                {
                    printf("A decimal value");
                } break;

                case FIELD_TYPE_YEAR:
                {
                    printf("'YYYY'");
                } break;

                case FIELD_TYPE_DATE:
                {
                    printf("'YYYY-MM-DD'");
                } break;

                case FIELD_TYPE_TIME:
                {
                    printf("'HH:MM:SS' or 'HHH:MM:SS'");
                } break;

                case FIELD_TYPE_DATETIME:
                {
                    printf("'YYYY-MM-DD HH:MM:SS'");
                } break;

                case FIELD_TYPE_STRING:
                {
                    printf("STRING");
                } break;

                case FIELD_TYPE_NULL:
                {

                } break;

                default:
                {
                    printf("Table attribute %s is of a non-supported datatype!\n", field->name);
                } break;
            }
            printf("\n");
            printf("Value: ");

            fgets(value, STRING_MAX, stdin);
            value[strcspn(value, "\n")] = 0;

            if(!ValidValueFormat(field->name, field->type, value))
            {
                printf("Please try again!\n");
                printf("\n");
                invalid_entry = 1;
            }
            else
            {
                (*values)[i] = malloc(sizeof(char) * (strlen(value) + 1));
                strcpy((*values)[i], value);

                PrettyFormat(STANDARDOUTPUT, terminalWidth);
                invalid_entry = 0;
            }
        } while(invalid_entry);

        i++;
    }

    free(value);
    for(i = 0; i < num_fields; i++)
    {
        free(database_attributes[i]);
    }
    free(database_attributes);
}

void QuoteValues(char** values, enum_field_types* typeArray, int numFields)
{
    for(int i = 0; i < numFields; i++)
    {
        switch(typeArray[i])
        {
            case FIELD_TYPE_TINY:
            case FIELD_TYPE_SHORT:
            case FIELD_TYPE_LONG:
            case FIELD_TYPE_INT24:
            case FIELD_TYPE_LONGLONG:
            case FIELD_TYPE_DECIMAL:
            case FIELD_TYPE_FLOAT:
            case FIELD_TYPE_DOUBLE:
            case FIELD_TYPE_YEAR:
            {} break; // Currently doing nothing to them

            case FIELD_TYPE_DATE:
            case FIELD_TYPE_TIME:
            case FIELD_TYPE_DATETIME:
            case FIELD_TYPE_STRING:
            {
                int str_len = strlen(values[i]);
                values[i] = realloc(values[i], sizeof(char) * ((str_len += 2) + 1)); // Add the null character and 2 quotes
                
                values[i][str_len-1] = '\'';
                for(int j = str_len-2; j > 1; j--)
                {
                    values[i][j] = values[i][j-1];
                }
                values[i][0] = '\'';
                values[i][str_len] = '\0';
            } break;

            case FIELD_TYPE_NULL:
            {} break;

            default:
            {} break;
        }
    }
}

void AddTupleToDB(MYSQL* mySQL, const char* databaseName, const char* tableName)
{
    MYSQL_RES* result;

    char query[QUERY_SIZE];
    sprintf(query, "SELECT * FROM %s.%s", databaseName, tableName);
    HandleQuery(mySQL, query);

    result = mysql_store_result(mySQL);

    int num_fields = mysql_num_fields(result);

    char** values = malloc(sizeof(char*) * num_fields);
    enum_field_types* type_array = malloc(sizeof(enum_field_types) * num_fields);

    GetTuple(mySQL, result, type_array, &values);

    QuoteValues(values, type_array, num_fields);

    char* tuple = malloc(sizeof(char));
    sprintf(tuple, "(");
    
    int tuple_length = 1;
    int num_commas = 0; // Indicate how many commas we currently have

    int curr_len;

    for(int i = 0; i < num_fields; i++)
    {
        // Add in space for comma, and if no comma then we will want space for ending parentheses ')'
        tuple_length += strlen(values[i]) + 1;

        tuple = realloc(tuple, sizeof(char) * (tuple_length + 1));

        // Add on to the end of our current string
        sprintf(tuple + strlen(tuple), "%s", values[i]);

        // If we are not on the last field, add a comma
        if(i < num_fields - 1)
            sprintf(tuple + strlen(tuple), ",");
        else
            sprintf(tuple + strlen(tuple), ")");

    }

    memset(query, 0, QUERY_SIZE);
    //TODO: database_attributes is used above, could just pass it in. NOTE: Will need to adjust QUERY SIZE FOR THIS TO WORK!
    //sprintf(query, "INSERT INTO %s%s VALUES %s", databaseName, database_attributes, tuple); 
    sprintf(query, "INSERT INTO %s.%s VALUES%s", databaseName, tableName, tuple);
    HandleQuery(mySQL, query);


    printf("Tuple added: %s\n", tuple);
    for(int i = 0; i < num_fields; i++)
    {
        free(values[i]);
    }
    free(values);
    free(tuple);
    free(type_array);
}

StoreList* CreateList()
{
    StoreList* storeList = malloc(sizeof(StoreList));
    
    storeList->price = 0;
    storeList->shop_time = 0;
    storeList->num_items = 0;

    return storeList;
}

void SaveList(StoreList* storeList, const char* filename, const char* mode)
{
    FILE* fp = fopen(filename, mode);

    PrintList(storeList, fp);

    printf("Wrote to file %s.\n", filename);
    fclose(fp);
}

void RemoveFromList(StoreList* storeList, const char* name)
{
    for(int i = 0; i < storeList->num_items; i++)
    {
        // Search for the item we wish to remove
        if( !strcmp(storeList->item[i].name, name) )
        {
            // Move over all the items
            for(int j = i; j < storeList->num_items-1; j++)
            {
                storeList->item[j] = storeList->item[j+1];
            }
            // Get rid of the last item since we now have one less
            storeList->num_items--;
            storeList->item = realloc(storeList->item, sizeof(Item) * storeList->num_items);
            return;
        }
    }

    printf("Item with name %s was not found!\n", name);
}

// TODO TODO: Condense down the dashes (-) to a single one (: make parsing easier
StoreList* LoadList(const char* filename)
{
    FILE* fp = fopen(filename, "r");
    ValidateFile(fp, filename);

    fseek(fp, 0L, SEEK_END);
    int num_bytes = ftell(fp);
    fseek(fp, 0L, SEEK_SET);

    char ch;
    uint1_t space_flag = 0;
    uint1_t dash_flag = 0;
    uint1_t write_flag = 0;

    char* data = malloc(sizeof(char) * 1);
    ValidateObject(data);

    int size = 0;

    while( (ch = getc(fp)) != EOF )
    {
        // If the character is something other than a space
        if( !isspace(ch) )
        {
            space_flag = 0;

            // Only care to write if either we don't have a dash or we have a dash but it is the first encounter
            if( ch != '-' || (ch == '-' && !dash_flag) )
            {
                if( ch == '-' )
                {
                    dash_flag = 1;
                }
                else
                {
                    dash_flag = 0;
                }
                
                data = realloc(data, sizeof(char) * (size+1));
                data[size++] = (char) ch;
                ValidateObject(data);
            }
        }
        // If the character is the first space add it to the data
        else if(ch == ' ' && !space_flag)
        {
            space_flag = 1;

            data = realloc(data, sizeof(char) * (size+1));
            data[size++] = (char) ch;
            ValidateObject(data);
        }
    }
    StoreList* storeList = ParseToList(data, size);

    free(data);
    fclose(fp);

    return storeList;
}

// TODO: SOmething breaks when I have an item with a name who's length is such that the string pushes into the next column
StoreList* ParseToList(char* data, int size)
{
    StoreList* storeList = CreateList();

    int clip_size = 0; // Variable so we can know how much to clip to get rid of column headers
    int num_attributes = 0;
    char ch = data[clip_size];
    // Parse through the attribute names and gather how many we are working with, remember this as where to start when parsing actual data
    while(ch != '-' && ch != '\n')
    {
        if(clip_size > size)
        {
            printf("Something is wrong, clip_size is larger than the size of the actual data to clip. Not proper formatted data?\n");
            printf("Data dump: %s\n", data);
            exit(1);
        }

        if(ch == ' ')
        {
            num_attributes++;
        }
        ch = data[++clip_size];
    }

    char** buffer = malloc(sizeof(char*) * num_attributes);

    for(int i = 0; i < num_attributes; i++)
    {
        buffer[i] = malloc(sizeof(char) * 1);
        buffer[i][0] = 0; // Initializing to 0 to serve as untouched flag
    }
    int buffer_length = 0;

    num_attributes = 0;

    // Start at point where we left off, go until end
    for( int i = clip_size-1; i < size; i++ )
    {
        // Fill the buffer
        if(data[i] != '-' && data[i] != ' ')
        {
            buffer[num_attributes] = realloc(buffer[num_attributes], sizeof(char) * (buffer_length + 1));
            buffer[num_attributes][buffer_length++] = data[i];
        }

        // Move to next buffer
        if(data[i] == ' ' && buffer[num_attributes][0])
        {
            // If we are dealing with a string, then add on the terminating string portion
            if(isalpha(buffer[num_attributes][0]))
            {
                buffer[num_attributes] = realloc(buffer[num_attributes], sizeof(char) * (buffer_length + 1));
                buffer[num_attributes][buffer_length] = '\0';
            }

            num_attributes++;
            buffer_length = 0;
        }

        // Empty the buffers (if there is contents in the buffers)
        if(data[i] == '-' && buffer[0][0])
        {
            // TODO: Currently I am assuming the order is "Name" "Price" and "Isle". Figure out a better way.
            if(!isalpha(buffer[0][0]) && !isdigit(buffer[1][0]) || !isdigit(buffer[2][0]))
            {
                printf("Unexpected attribute type! Order is currently Name - Price - Isle as String - Double - Int\n");
                printf("Dump: buffer[0] = %s\tbuffer[1] = %s\tbuffer[2] = %s\n", buffer[0][0], buffer[1][0], buffer[2][0]);
                exit(1);
            }

            Item* item = CreateItem( (const char*) buffer[0], atof(buffer[1]), atoi(buffer[2]) );
            AddItem(storeList, item);

            for(int j = 0; j < num_attributes; j++)
            {
                buffer[j][0] = 0; // Reset each buffer to be null to be used as flag
            }
            num_attributes = 0;
            buffer_length = 0;
        }
    }
    
    return storeList;
}

void DeleteList(StoreList* storeList)
{
    if(storeList->num_items > 0)
    {
        for(int i = 0; i < storeList->num_items; i++)
        {
            free(storeList->item[i].name);
            //free(storeList->item[i]);
            storeList->num_items--;
        }
        free(storeList->item);
    }

    storeList->num_items = 0;
    storeList->price = 0;
    storeList->shop_time = 0;

    free(storeList);
}

Item* CreateItem(const char* name, double price, int isle)
{
    Item* item = malloc(sizeof(Item));

    item->name = malloc(sizeof(char) * (strlen(name) + 1));
    strcpy(item->name, name);

    item->price = price;
    item->isle = isle;

    return item;
}

void DeleteItem(Item* item)
{
    free(item->name);
    item->price = 0;
    item->isle = 0;

    free(item);
}

// TODO: Check... Is the item setting correct?
void AddItem(StoreList* storeList, Item* item)
{
    if(storeList->num_items == 0)
    {
        storeList->item = malloc(sizeof(item));
    }

    int num_items = storeList->num_items++;
    
    storeList->item = realloc(storeList->item, sizeof(Item) * storeList->num_items);
    storeList->item[num_items] = *item;
}

void PrintList(StoreList* storeList, FILE* fileStream)
{
    int numTerminalSplit = 3;
    int widthPerColumn = terminalWidth / numTerminalSplit;

    printf("\n");
    //PrettyFormat(fileStream, terminalWidth);
    fprintf(fileStream, "%-*s", widthPerColumn, "Name");
    fprintf(fileStream, "%-*s", widthPerColumn, "Price");
    fprintf(fileStream, "%-*s", widthPerColumn, "Isle");
    PrettyFormat(fileStream, terminalWidth);

    for(int i = 0; i < storeList->num_items; i++)
    {
        fprintf(fileStream, "%-*s", widthPerColumn, storeList->item[i].name);
        fprintf(fileStream, "%-*.2f", widthPerColumn, storeList->item[i].price);
        fprintf(fileStream, "%-*i", widthPerColumn, storeList->item[i].isle);
        PrettyFormat(fileStream, terminalWidth);
        //fprintf(fileStream, "\n");
    }
    fprintf(fileStream, "\n");
}









